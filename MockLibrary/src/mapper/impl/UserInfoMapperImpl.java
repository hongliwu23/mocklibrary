/**
 * 
 */
package mapper.impl;

import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import domain.User;
import mapper.UserInfoMapper;

/**
 * @author wuhongli
 *
 */
public class UserInfoMapperImpl implements UserInfoMapper {

	@Override
	public Integer loginCheck(User user) {
		// TODO Auto-generated method stub
		
		Configuration config = new Configuration();
		config.configure("hibernate.cfg.xml");
		SessionFactory sessionFactory = config.buildSessionFactory();
		Session session = sessionFactory.openSession();
		
		String hql = "Select userId from User where user_name=:userName and user_password=:passWord";  
		Query query = session.createQuery(hql);
		query.setParameter("userName", user.getUserName());
		query.setParameter("passWord", user.getPassWord());
		List result = query.list();
		
		Iterator iterator = result.iterator();

		if(iterator.hasNext()) {
			return (Integer) iterator.next();
		}
		else {
			return 0;
		}
	}

	@Override
	public Boolean logoutRecord(User user) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public User getUserInfo(User user) {
		// TODO Auto-generated method stub
		Configuration config = new Configuration();
		config.configure("hibernate.cfg.xml");
		SessionFactory sessionFactory = config.buildSessionFactory();
		Session session = sessionFactory.openSession();
		
		Object object = session.load(User.class, user.getUserId());
		User userInfo = (User) object;
		
		
		return userInfo;
	}

}
