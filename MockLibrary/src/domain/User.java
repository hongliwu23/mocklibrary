/**
 * 
 */
package domain;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * @author wuhongli
 *
 */
@Entity
public class User {

	@Id
	private int userId;
	private String userName;
	private String fullName;
	private String passWord;
	private Timestamp enrollDate;
//	private int roleId;
	
	@ManyToOne
	@JoinColumn(name="role_id")
	private Role role;
	
	@ManyToOne
	@JoinColumn(name="class_id")
	private Class userClass;
	
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getPassWord() {
		return passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public Timestamp getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(Timestamp enrollDate) {
		this.enrollDate = enrollDate;
	}

//	public int getRoleId() {
//		return roleId;
//	}
//
//	public void setRoleId(int roleId) {
//		this.roleId = roleId;
//	}

	public Role getRole() {
		if(role == null) {
			role = new Role();
			role.setRoleId(0);
			role.setRoleName("Administrator");
			role.setBorrowNum(20);
		}
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Class getUserClass() {
		return userClass;
	}

	public void setUserClass(Class userClass) {
		this.userClass = userClass;
	}

	
}
