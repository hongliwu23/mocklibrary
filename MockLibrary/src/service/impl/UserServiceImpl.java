package service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import domain.User;
import mapper.UserInfoMapper;
import service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService{

	@Autowired
	private UserInfoMapper userInfo;
	
	public UserInfoMapper getUserInfo() {
		return userInfo;
	}

	public void setUserInfo(UserInfoMapper userInfo) {
		this.userInfo = userInfo;
	}

	@Override
	public Integer loginAuthenticate (Map<String,String> map) {
		Integer result = 0;
		
		User user = new User();
		user.setUserName(map.get("userName"));
		user.setPassWord(map.get("passWord"));
		
		result = userInfo.loginCheck(user);
		
		return result;
		
	}
	
	public User getUserInfo(Integer userId) {
		User user = new User();
		user.setUserId(userId);
		
		User currentUser = userInfo.getUserInfo(user);
		
		return currentUser;
		
	}
}
