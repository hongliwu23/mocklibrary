/**
 * 
 */
package controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import domain.User;
import service.UserService;

/**
 * @author wuhongli
 *
 */
@Controller
public class LoginController {

	private String message;
	
	@Autowired
	private UserService userService;
	
	public UserService getUserService() {
		return userService;
	}


	public void setUserService(UserService userService) {
		this.userService = userService;
	}


	/**
	 * Return to index page
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping("/index.htm")
	public String LoginRequest(Model model,
			HttpServletRequest request,
			HttpServletResponse response) {
		
		String userName = request.getParameter("userName");
		String passWord = request.getParameter("passWord");
		System.out.println("use request: "+message);
		System.out.println("user "+userName+" psw "+passWord);
		model.addAttribute("welcomeMessage", message);
		
		Map<String,String> map = new HashMap<String,String>();
		map.put("userName", userName);
		map.put("passWord", passWord);
		
		Integer userId = userService.loginAuthenticate(map) ;
		
		if (userId > 0 ) {
			User userInfo = userService.getUserInfo(userId);
			System.out.println("roleid "+userInfo.getRole().getRoleId());
			
			
			if(userInfo.getRole().getRoleId()==0) {
				return "admin/homepage";
			}
			if(userInfo.getRole().getRoleId()==1) {
				model.addAttribute("userRole", userInfo.getRole().getRoleName());
				model.addAttribute("fullName", userInfo.getFullName());
				model.addAttribute("userClass", userInfo.getUserClass().getClassName());
				return "teacher/homepage";
			}
			if(userInfo.getRole().getRoleId()==2) {
				model.addAttribute("userRole", userInfo.getRole().getRoleName());
				model.addAttribute("fullName", userInfo.getFullName());
				model.addAttribute("userClass", userInfo.getUserClass().getClassName());
				return "student/homepage";
			}
			
			return "success";
		
		}
		
		return "error";
	}
	
	
	public void setMessage(String message) { 
		this.message = message;
	} 
}
