/**
 * 
 */
package mapper;

import domain.User;

/**
 * @author wuhongli
 *
 */

public interface UserInfoMapper {

	
	public Integer loginCheck(User user);
	
	public Boolean logoutRecord(User user);
	
	public User getUserInfo(User user);
}
