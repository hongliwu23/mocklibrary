/**
 * 
 */
package service;

import java.util.Map;

import domain.User;

/**
 * @author wuhongli
 *
 */
public interface UserService {

	public Integer loginAuthenticate (Map<String,String> map);
	
	public User getUserInfo(Integer userId);
}
