/**
 * 
 */
package domain;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author wuhongli
 *
 */
@Entity
public class Role {

	@Id
	private int roleId;
	private String roleName;
	private int borrowNum;
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public int getBorrowNum() {
		return borrowNum;
	}
	public void setBorrowNum(int borrowNum) {
		this.borrowNum = borrowNum;
	}
	
	
}
